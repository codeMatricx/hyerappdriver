package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.DeleteEarning_Request;
import com.hyr_driver.responseParser.DeleteEarning_Response;
import com.hyr_driver.responseParser.MyEarning;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.List;


/**
 * Created by iSiwal on 1/27/2018.
 */

public class MyEarnings_Adapter extends BaseAdapter {
    Context context;
    List<MyEarning> list;


    public MyEarnings_Adapter(Context context, List<MyEarning> list)
    {
        this.context = context;
        this.list=list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MyEarnings_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.myearning_adap, null);
            holder = new MyEarnings_Adapter.Holder();
            holder.amount=(TextView)convertView.findViewById(R.id.tv_amounts);
            holder.Date=(TextView)convertView.findViewById(R.id.tv_date);
            holder.source=(TextView)convertView.findViewById(R.id.tv_source);
            holder.destination=(TextView)convertView.findViewById(R.id.tv_destination);
            holder.images_delete=(ImageView)convertView.findViewById(R.id.img_delete);
            holder.images_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer index = (Integer) v.getTag();
                    //items.remove(index.intValue());
                    list.remove(position);
                    notifyDataSetChanged();
                    new Deleteearning().execute();

                }
            });

            convertView.setTag(holder);
        }
        else
        {
            holder = (MyEarnings_Adapter.Holder) convertView.getTag();
        }

        holder.amount.setText(list.get(position).getDayEarning());
        holder.Date.setText(list.get(position).rideDate);
        holder.source.setText(list.get(position).source);
        holder.destination.setText(list.get(position).destination);



        return convertView;

    }

    public class Holder {
        TextView amount,Date,source,destination;
        ImageView images_delete;


    }


    private class Deleteearning extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private DeleteEarning_Response deleteResponseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(context, "", "Deleting...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                deleteResponseparser = gson.fromJson(response, DeleteEarning_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (deleteResponseparser != null) {
                if (deleteResponseparser.code.trim().equals("200")) {

                    Toast.makeText(context, deleteResponseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(deleteResponseparser.message.toString().trim(),
                            (Activity) context);
                }
            }
//          else {
//                CommonMethod.showAlert(getResources().getString(R.string.connection_error), context);
//            }
        }


        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.Delete_Earning;
            Log.e("url----------------", "" + url);
            try {
                String driver=CommonMethod.getSavedPreferencesDriverID(context);
                String booking=CommonMethod.getsaveBookingidEarning(context);
                DeleteEarning_Request deleteRequestParser = new DeleteEarning_Request();
                String bookingid=deleteRequestParser.bookingId= booking;
                String drivers=deleteRequestParser.driverId=driver;

                Log.e("get Driver id----","" + drivers);
                Log.e("get Booking id----","" + bookingid);

                Gson gson = new Gson();

                String requestInString = gson.toJson(deleteRequestParser, DeleteEarning_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(context, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
