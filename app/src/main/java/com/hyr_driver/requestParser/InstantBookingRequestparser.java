package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/28/2017.
 */

public class InstantBookingRequestparser {
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("number_passenger")
    @Expose
    public String numberPassenger;
    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;
    @SerializedName("ride_type")
    @Expose
    public String rideType;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
}
