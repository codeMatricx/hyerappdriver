package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/13/2018.
 */

public class Reject_RequestParser {
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
}
