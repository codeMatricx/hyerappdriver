package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/22/2017.
 */

public class CompleteRide_Requestparser {

    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
}
