package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/27/2018.
 */

public class MyEarning_Request {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("ride_date")
    @Expose
    public String rideDate;
}
