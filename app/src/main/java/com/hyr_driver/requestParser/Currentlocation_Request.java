package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/25/2017.
 */

public class Currentlocation_Request {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("isdutyon")
    @Expose
    public String isdutyon;
}
