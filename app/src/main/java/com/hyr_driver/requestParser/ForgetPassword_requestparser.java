package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/20/2017.
 */

public class ForgetPassword_requestparser {
    @SerializedName("email")
    @Expose
    public String email;
}
