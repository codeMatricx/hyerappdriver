package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/20/2017.
 */

public class RegisterRequestparser {
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("driving_license")
    @Expose
    public String drivingLicense;
}
