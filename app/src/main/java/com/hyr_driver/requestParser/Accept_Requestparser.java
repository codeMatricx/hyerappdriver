package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/12/2018.
 */

public class Accept_Requestparser {
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
}
