package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/27/2018.
 */

public class DeleteEarning_Request {
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
}
