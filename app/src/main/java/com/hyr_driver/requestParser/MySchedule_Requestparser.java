package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/25/2018.
 */

public class MySchedule_Requestparser {
    @SerializedName("driverId")
    @Expose
    public String driverId;
    @SerializedName("bookingDate")
    @Expose
    public String bookingDate;
}
