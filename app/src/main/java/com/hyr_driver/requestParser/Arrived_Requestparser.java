package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/22/2017.
 */

public class Arrived_Requestparser {

    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
}
