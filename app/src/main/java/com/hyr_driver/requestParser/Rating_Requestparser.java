package com.hyr_driver.requestParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/13/2018.
 */

public class Rating_Requestparser {
    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("rating")
    @Expose
    public String rating;
}
