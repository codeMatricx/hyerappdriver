package com.hyr_driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.Extras.DirectionFinder;
import com.hyr_driver.Extras.DirectionFinderListener;
import com.hyr_driver.Extras.Route;
import com.hyr_driver.requestParser.StartTrip_RequestParser;
import com.hyr_driver.responseParser.StartTrip_Responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class After_Arrived extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,DirectionFinderListener {


    Toolbar toolbar;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int DPLACE_PICKER_REQUEST = 2;
    String newsource,newdesti,currentocation;

    private GoogleMap mMap;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private static final String LOG_TAG = "PlaceSelectionListener";
    private ArrayList<LatLng> point; //added
    Polyline line; //added
    private int PROXIMITY_RADIUS = 10000;
    Context context;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    String src_latlang,desti_latlang,address;
    Switch defaultSwitch;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    double latitude;
    double longitude;
    Button btn_starttrip;
    FrameLayout container;
    String pas_destination,address_destination,driverides,bookingides;
    TextView tv_source,tv_destination;
    TextView cancel_tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_after__arrived);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        pas_destination=CommonMethod.getsavePassengerDestination(After_Arrived.this);
        cancel_tv=(TextView)findViewById(R.id.canceltrip);
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Are you sure want to Cancel this trip");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent n = new Intent(After_Arrived.this,MainActivity.class);
                                startActivity(n);
                                finish();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
        driverides=CommonMethod.getSavedPreferencesDriverID(After_Arrived.this);
        bookingides=CommonMethod.getSavedBookingIDMain(After_Arrived.this);
        currentocation=CommonMethod.getSavedPreferencesRide_source(After_Arrived.this);
        Log.e("Location",""+currentocation);

        tv_source=(TextView)findViewById(R.id.instant_tv_source);
        tv_source.setText(currentocation);
        tv_destination=(TextView)findViewById(R.id.instant_tv_desti);
        tv_destination.setText(pas_destination);
        btn_starttrip=(Button)findViewById(R.id.btn_startrips);
        btn_starttrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsource=tv_source.getText().toString();
                newdesti=tv_destination.getText().toString();
                CommonMethod.saveNewSource(getApplicationContext(),newsource);
                CommonMethod.saveNewdESTINATION(getApplicationContext(),newdesti);
                if(newdesti!="") {

                    new starttrip().execute();
                }
                else {
                    Toast.makeText(getApplicationContext(),"Enter Destination",Toast.LENGTH_LONG).show();
                }

            }
        });


        tv_source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(After_Arrived.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
        tv_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(After_Arrived.this), DPLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        context = this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            finish();
        }
        else {
            Log.d("onCreate","Google Play Services available.");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Initializing
        mapFragment.getMapAsync(this);


    }

//------------------------------------OnCreate finished---------------------------//
private void sendRequest() {
    String origin = src_latlang;
    String destinations = desti_latlang;
    if (origin.isEmpty()) {
        Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
        return;
    }
    if (destinations.isEmpty()) {
        Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
        return;
    }

    try {
        new DirectionFinder(this, origin, destinations).execute();
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
}



    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);



            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                tv_source.setText(address + " " + city + " " + country);
                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }






    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                address =String.format("%s", place.getAddress());
                stBuilder.append(address);
                //tv_source.setText(stBuilder.toString());
                CommonMethod.saveRide_source(getApplicationContext(),address);
                src_latlang=(latitude+","+longitude);
            }
        }

        if (requestCode == DPLACE_PICKER_REQUEST) {
            Place place = PlacePicker.getPlace(data, this);
            StringBuilder stBuilder = new StringBuilder();
            //String placename = String.format("%s", place.getName());
            String latitudes = String.valueOf(place.getLatLng().latitude);
            String longitudes = String.valueOf(place.getLatLng().longitude);
            address_destination = String.format("%s", place.getAddress());
            stBuilder.append(address_destination);
            tv_destination.setText(stBuilder.toString());
            CommonMethod.saveRide_destination(getApplicationContext(),address_destination);
            desti_latlang=(latitudes+","+longitudes);
            if(address_destination != null && address!=null)
            {
                sendRequest();
            }
        }
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }



    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 7));
            // ((TextView) findViewById(R.id.tv_etime)).setText(route.duration.text);
            //((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLACK).
                    width(6);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private class starttrip extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private StartTrip_Responseparser mStartTrip_Responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(After_Arrived.this, "", "Starttrip...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mStartTrip_Responseparser = gson.fromJson(response, StartTrip_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mStartTrip_Responseparser != null) {
                if (mStartTrip_Responseparser.code.trim().equals("200")) {

                    Intent intent = new Intent(After_Arrived.this, Complete_Ride.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(After_Arrived.this, mStartTrip_Responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mStartTrip_Responseparser.message.toString().trim(),
                            After_Arrived.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), After_Arrived.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.StartTrips;
            Log.e("url----------------", "" + url);
            try {
                StartTrip_RequestParser mStartTrip_RequestParser = new StartTrip_RequestParser();
                String driverid=mStartTrip_RequestParser.driverId=driverides;
                String bookinid =mStartTrip_RequestParser.bookingId=bookingides;

                Log.e("Driverid----","" + driverid);
                Log.e("Bookingid---","" + bookinid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mStartTrip_RequestParser, StartTrip_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(After_Arrived.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
