package com.hyr_driver;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.hyr_driver.utils.Session;

/**
 * Created by iSiwal on 9/22/2017.
 */

public class After_Splash extends AppCompatActivity {
    Button btnsign, btn_register;
    Toolbar toolbartop;
    AlertDialogManager alert = new AlertDialogManager();
    Context context;
    TextView tv_forgetpasssword;
    Session s1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getActionBar().hide();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_register);
        s1=new Session(this);
        if(s1.loggedin())
        {
            checkInternetConenction();
        }

        tv_forgetpasssword=(TextView)findViewById(R.id.tv_forgotpass);
        tv_forgetpasssword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fpass= new Intent(After_Splash.this,Forgotpass.class);
                startActivity(fpass);
            }
        });


        btnsign=(Button)findViewById(R.id.btn_signIn);
        btnsign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkInternetConenction();
            }
        });
        btn_register=(Button)findViewById(R.id.btn_registers);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(After_Splash.this,Register_Activity.class);
                startActivity(j);
            }
        });

    }
    public boolean checkInternetConenction() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec
                =(ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() ==
                android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() ==
                        android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {
            Intent i = new Intent(After_Splash.this,Login_Activity.class);
            startActivity(i);
            //Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;
        }else if (
                connec.getNetworkInfo(0).getState() ==
                        android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() ==
                                android.net.NetworkInfo.State.DISCONNECTED  ) {
            alert.showAlertDialog(After_Splash.this, "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return

            return false;
        }
        return false;
    }


}
