package com.hyr_driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Insurance extends AppCompatActivity {
    ImageView viewImage;
    Button b;
    Toolbar toolbartop;

    ImageView imageView;
    private int SELECT_FILE;
    private int REQUEST_CAMERA;
    Bitmap imag1;
    ImageView circleImageView;
    ImageView circleImageView2;
    private Bitmap thumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insurance);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        b=(Button)findViewById(R.id.btn_capture);
        viewImage=(ImageView)findViewById(R.id.image);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        toolbartop = (Toolbar) findViewById(R.id.toolbar_insurance);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds options to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }





    private void selectImage() {

        final CharSequence[] items = {"Take Photo", "Choose from Library","Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(Insurance.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(Insurance.this);

                String userChoosenTask;
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntentt();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntentt();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntentt() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntentt() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

@Override
public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
        if (resultCode == Activity.RESULT_OK) {
        if (requestCode == SELECT_FILE)
        onSelectFromGalleryResult(data);
        else if (requestCode == REQUEST_CAMERA)
        onCaptureImageResult(data);

        }
        }
        if (requestCode == 0) {
        if (resultCode == Activity.RESULT_OK) {
        if (requestCode == SELECT_FILE)
        onSelectFromGalleryResult(data);
        if (requestCode == REQUEST_CAMERA)
        onCaptureImageResult(data);

        }
        }
        if (requestCode == 1) {
        if (resultCode == Activity.RESULT_OK) {
        if (requestCode == SELECT_FILE)
        onImageSelectFromGalleryResult(data);
        else if (requestCode == REQUEST_CAMERA)
        onImageCaptureImageResult(data);

        }
        }

        }




private void onCaptureImageResult(Intent data) {
        imag1 = (Bitmap) data.getExtras().get("data");
        AppData.getSingletonObject().setThumbnailPostCreater(imag1);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        imag1.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
        System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
        destination.createNewFile();
        fo = new FileOutputStream(destination);
        fo.write(bytes.toByteArray());
        fo.close();
        } catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }
        circleImageView.setImageBitmap(imag1);

        }

private void onSelectFromGalleryResult(Intent data) {

        if (data != null) {
        try {
        imag1 = MediaStore.Images.Media.getBitmap(Insurance.this.getContentResolver(), data.getData());
        AppData.getSingletonObject().setThumbnailPostCreater(imag1);
        } catch (IOException e) {
        e.printStackTrace();
        }
        }
        circleImageView.setImageBitmap(imag1);

        }



private void onImageCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        AppData.getSingletonObject().setThumbnailPostCreater(thumbnail);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
        System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
        destination.createNewFile();
        fo = new FileOutputStream(destination);
        fo.write(bytes.toByteArray());
        fo.close();
        } catch (FileNotFoundException e) {
        e.printStackTrace();
        } catch (IOException e) {
        e.printStackTrace();
        }
        circleImageView2.setImageBitmap(thumbnail);

        }

private void onImageSelectFromGalleryResult(Intent data) {

        if (data != null) {
        try {
        thumbnail = MediaStore.Images.Media.getBitmap(Insurance.this.getContentResolver(), data.getData());
        AppData.getSingletonObject().setThumbnailPostCreater(thumbnail);
        } catch (IOException e) {
        e.printStackTrace();
        }
        }
        circleImageView2.setImageBitmap(thumbnail);

        }

    }

