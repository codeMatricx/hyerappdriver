package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.OtpRequestParser;
import com.hyr_driver.responseParser.OtpResponseParser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;


import org.apache.http.entity.StringEntity;

public class Activity_otp extends AppCompatActivity implements View.OnClickListener{

    Button btnNext;
    EditText et1,et2,et3,et4;
    ImageView back;
    TextView title;
    private View v;
    Toolbar toolbartop;
    TextView mTitle;
    String a,b,c,d,mergeotp,userphone;


    protected void onCreate(Bundle savedInseTanceState){
        super.onCreate(savedInseTanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_otp);
        initialization();
        btnNext.setOnClickListener(this);
        final StringBuilder str=new StringBuilder();
        toolbartop = (Toolbar) findViewById(R.id.toolbar_otps);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        userphone=CommonMethod.getSavedRegisterphone(Activity_otp.this);
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(et1.getText().toString().length()==1){
                    str.append(s);
                    et1.clearFocus();
                    et2.requestFocus();
                    et2.setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(et2.getText().toString().length()==1){
                    str.append(s);
                    et2.clearFocus();
                    et3.requestFocus();
                    et3.setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(et2.getText().toString().isEmpty()){
                    et2.clearFocus();
                    et1.requestFocus();
                    et1.setCursorVisible(true);
                }

            }
        });



        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(et3.getText().toString().length()==1){
                    str.append(s);
                    et3.clearFocus();
                    et4.requestFocus();
                    et4.setCursorVisible(true);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(et3.getText().toString().isEmpty()){
                    et3.clearFocus();
                    et2.requestFocus();
                    et2.setCursorVisible(true);
                }

            }
        });


        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(et4.getText().toString().isEmpty()){
                    et4.clearFocus();
                    et3.requestFocus();
                    et3.setCursorVisible(true);
                }

            }
        });
    }

    private void initialization() {
        btnNext= (Button) findViewById(R.id.a_otp_btn_next);
        title= (TextView) findViewById(R.id.title);
        //back= (ImageView) findViewById(R.id.custom_toolbar_icon_back);
        et1= (EditText) findViewById(R.id.a_otp_et1);
        et2= (EditText) findViewById(R.id.a_otp_et2);
        et3= (EditText) findViewById(R.id.a_otp_et3);
        et4= (EditText) findViewById(R.id.a_otp_et4);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.a_otp_btn_next:
                a=et1.getText().toString();
                b=et2.getText().toString();
                c=et3.getText().toString();
                d=et4.getText().toString();
                mergeotp = a + "" + b + "" + c + "" +d;

                if(et1.getText().toString().length()==0){
                    et1.setError("Please enter valid OTP");
                    et1.requestFocus();
                }

                else if(et2.getText().toString().length()==0){
                    et2.setError("Please enter valid OTP");
                    et2.requestFocus();
                }

                else if(et3.getText().toString().length()==0){
                    et3.setError("Please enter valid OTP");
                    et3.requestFocus();
                }
                else if(et4.getText().toString().length()==0){
                    et4.setError("Please enter valid OTP");
                    et4.requestFocus();
                }

               else{
                    Toast.makeText(Activity_otp.this, ""+mergeotp, Toast.LENGTH_LONG).show();
                    new otpverification().execute();
                }

                break;
        }
    }
    private class otpverification extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private OtpResponseParser mOtpResponseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Activity_otp.this, "", "Verifying...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mOtpResponseparser = gson.fromJson(response, OtpResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mOtpResponseparser != null) {
                if (mOtpResponseparser.responseCode.trim().equals("200")) {
                    Intent intent = new Intent(Activity_otp.this, Login_Activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Activity_otp.this, mOtpResponseparser.responseMessage, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mOtpResponseparser.responseMessage.toString().trim(),
                            Activity_otp.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Activity_otp.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.OTP;
            Log.e("url----------------", "" + url);
            try {
                OtpRequestParser mOtpRequestParser = new OtpRequestParser();
                String otps=mOtpRequestParser.otp = mergeotp;
                String phones =mOtpRequestParser.phone= userphone;

                Log.e("Otp----------------","" + otps);
                Log.e("Phone----------------","" + phones);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mOtpRequestParser, OtpRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Activity_otp.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
