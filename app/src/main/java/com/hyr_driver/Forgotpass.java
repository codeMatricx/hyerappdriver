package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.ForgetPassword_requestparser;
import com.hyr_driver.responseParser.Forgetpass_responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;


public class Forgotpass extends Activity {
Toolbar toolbartop;
    Button btn_send;
    EditText et_email;
    String emailid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forgotpass);
        toolbartop =(Toolbar) findViewById(R.id.toolbar_fpassword);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        et_email=(EditText)findViewById(R.id.forgetpassword);
        btn_send=(Button)findViewById(R.id.register_btn);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                new forgetAsync().execute();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private class forgetAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Forgetpass_responseparser mForgetpass_responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Forgotpass.this, "", "Sending to your Email id.", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mForgetpass_responseparser = gson.fromJson(response, Forgetpass_responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mForgetpass_responseparser != null) {
                if (mForgetpass_responseparser.code.trim().equals("200")) {

                    AlertDialog.Builder dialog = new AlertDialog.Builder(Forgotpass.this);
                    dialog.setCancelable(false);
                    dialog.setTitle("Thank You !!!");
                    dialog.setMessage("Your password has been changed Successfully, Please check your Email");
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Forgotpass.this, Login_Activity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            Toast.makeText(Forgotpass.this, mForgetpass_responseparser.message, Toast.LENGTH_SHORT).show();
                        }
                    });


                    AlertDialog alertDialog = dialog.create();
                    alertDialog.show();




                } else {
                    CommonMethod.showAlert(mForgetpass_responseparser.message.toString().trim(),
                            Forgotpass.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Forgotpass.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.FORGETPASS;
            Log.e("url----------------", "" + url);
            try {
                ForgetPassword_requestparser mForgetPassword_requestparser = new ForgetPassword_requestparser();

                emailid = mForgetPassword_requestparser.email= et_email.getText().toString().trim();

                Log.e("email----------------","" + emailid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mForgetPassword_requestparser, ForgetPassword_requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Forgotpass.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
