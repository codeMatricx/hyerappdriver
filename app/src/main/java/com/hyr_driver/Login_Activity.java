package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.Extras.SessionManager;
import com.hyr_driver.requestParser.LoginRequestParser;
import com.hyr_driver.responseParser.LoginResponseParser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;
import com.hyr_driver.utils.DbHelper;
import com.hyr_driver.utils.Session;

import org.apache.http.entity.StringEntity;

public class Login_Activity extends Activity {
Button btn_logins;
    Toolbar toolbartop;
    EditText edit_phone,edit_password;
    String s_phone,s_pass,st_name,st_email,st_phonenum,st_driver_id,st_password,s_devicetoken,device_token;
    private DbHelper db;
    SessionManager session;
    private Session set_s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_);
        set_s=new Session(this);
        session = new SessionManager(getApplicationContext());
        session = new SessionManager(Login_Activity.this);
        edit_phone = (EditText) findViewById(R.id.et_phone);
        edit_password = (EditText) findViewById(R.id.et_pass);
        btn_logins=(Button)findViewById(R.id.btnLogin);
        device_token=CommonMethod.getsaveDeviceToken(Login_Activity.this);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_loginid);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent n =new Intent(Login_Activity.this,After_Splash.class);
                startActivity(n);
                finish();
            }
        });

        btn_logins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit_phone.getText().toString().length()==0){
                    edit_phone.setError("Please Enter Registered Phone number");
                    edit_phone.requestFocus();
                }
                 else if(edit_password.getText().toString().length()<=5){
                    edit_password.setError("Please Enter Registered Password");
                    edit_password.requestFocus();
                }
                else {
                    new SigninAsync().execute();
                }

            }
        });

        if(set_s.loggedin()){
            startActivity(new Intent(Login_Activity.this,MainActivity.class));
            finish();
        }
    }
    private class SigninAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private LoginResponseParser mLoginResponseParser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Login_Activity.this, "", "Login...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mLoginResponseParser = gson.fromJson(response, LoginResponseParser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mLoginResponseParser != null) {
                if (mLoginResponseParser.code.trim().equals("200")) {
                    st_name=mLoginResponseParser.profile.name;
                    st_email=mLoginResponseParser.profile.email;
                    st_phonenum=mLoginResponseParser.profile.phone;
                    st_driver_id=mLoginResponseParser.profile.userId;
                    st_password=mLoginResponseParser.profile.password;

                    session.createLoginSession(st_phonenum, st_password);
                    set_s.setLoggedin(true);
                    CommonMethod.saveUserName(getApplicationContext(),st_name);
                    CommonMethod.saveEmailid(getApplicationContext(),st_email);
                    CommonMethod.savePhone(getApplicationContext(),st_phonenum);
                    CommonMethod.saveDriverID(getApplicationContext(),st_driver_id);

                    Log.e("++++USERNAME++ -- ",""+ st_name);
                    Log.e("+++USEREMAIL+ -- ",""+ st_email);
                    Log.e("++++USERPHONE++ -- ",""+ st_phonenum);
                    Log.e("++++driverid++ -- ",""+ st_driver_id);

                    Intent intent = new Intent(Login_Activity.this, MainActivity.class);
                    intent.putExtra("Name",st_name);
                    intent.putExtra("Phone",st_phonenum);
                    intent.putExtra("Email",st_email);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Login_Activity.this, mLoginResponseParser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mLoginResponseParser.message.toString().trim(),
                            Login_Activity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Login_Activity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.LOGIN;
            Log.e("url----------------", "" + url);
            try {
                LoginRequestParser mLoginRequestParser = new LoginRequestParser();

                s_phone=mLoginRequestParser.phone = edit_phone.getText().toString().trim();
                s_pass=mLoginRequestParser.password = edit_password.getText().toString().trim();
                mLoginRequestParser.deviceType="android";
                s_devicetoken= mLoginRequestParser.deviceToken=device_token;

                Log.e("phone----------------","" + s_phone);
                Log.e("passwor----------------","" + s_pass);
                Log.e("DEviceToken-----","" + s_devicetoken);


                Gson gson = new Gson();
                String requestInString = gson.toJson(mLoginRequestParser, LoginRequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Login_Activity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

