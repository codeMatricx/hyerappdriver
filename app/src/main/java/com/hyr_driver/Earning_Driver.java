package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.MyEarning_Request;
import com.hyr_driver.responseParser.MyEarning;
import com.hyr_driver.responseParser.MyEarning_Response;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;
import org.apache.http.entity.StringEntity;
import java.util.Calendar;


public class Earning_Driver extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbartop;
    ListView listview;
    Button btnDatePicker, btnTimePicker,submit;
    EditText txtDate, txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TextView tv_earning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_earning__driver);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_myearnings);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        listview=(ListView)findViewById(R.id.myearning_list);
        btnDatePicker=(Button)findViewById(R.id.btn_date);
        txtDate=(EditText)findViewById(R.id.in_date);
        submit=(Button)findViewById(R.id.submit_ridelater);
        tv_earning=(TextView)findViewById(R.id.tv_totalearnings);

        // Api Hits in oncreate method
        new MyEarnings().execute();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDate.getText().toString().length()==0){
                    txtDate.setError("Select Date");
                    txtDate.requestFocus();
                }

                else{
                    new MyEarnings().execute();
                }

            }
        });

        btnDatePicker.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

    }

    private class MyEarnings extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private MyEarning_Response myEarning_response;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Earning_Driver.this, "", "Please Wait...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                myEarning_response = gson.fromJson(response, MyEarning_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (myEarning_response != null) {
                if (myEarning_response.code.trim().equals("200")) {
                    String totalearning= myEarning_response.totalEarning;
                    tv_earning.setText(totalearning);
                    MyEarnings_Adapter myEarnings_adapter = new MyEarnings_Adapter(Earning_Driver.this, myEarning_response.myEarning);
                    listview.setAdapter(myEarnings_adapter);
                    myEarnings_adapter.notifyDataSetChanged();
                    for (int i = 0; i < myEarning_response.myEarning.size(); i++) {
                        MyEarning model = myEarning_response.myEarning.get(i);
                        String Earning_driver = model.getBookingId();
                        Log.e("Contactid", "" + Earning_driver);
                        CommonMethod.saveBookingidEarning(getApplicationContext(), Earning_driver);

                    }

                } else {
                    CommonMethod.showAlert(myEarning_response.message.toString().trim(), Earning_Driver.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Earning_Driver.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.Earning;
            Log.e("url----------------", "" + url);
            try {
                String driver_id= CommonMethod.getSavedPreferencesDriverID(Earning_Driver.this);
                MyEarning_Request myEarning_request = new MyEarning_Request();
                String driverides=myEarning_request.userId=driver_id;
                String bookingdate= myEarning_request.rideDate=txtDate.getText().toString();


                Log.e("Driver_ides----","" + driverides);
                Log.e("Booking_Date---","" + bookingdate);



                Gson gson = new Gson();
                String requestInString = gson.toJson(myEarning_request, MyEarning_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Earning_Driver.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
