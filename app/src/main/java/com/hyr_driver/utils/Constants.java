package com.hyr_driver.utils;

/**
 * Created by iSiwal on 12/1/2017.
 */

public class Constants {
    public static final java.lang.String PAYMENT_BUNDLE = "payment_bundle";
    public static final java.lang.String ORDER_ID = "orderId";
    public static final java.lang.String ORDER = "order";
    public static final int REQUEST_CODE = 9;
    public static final java.lang.String URL = "url";
    public static final java.lang.String MERCHANT_ID = "merchantId";
    public static final java.lang.String POST_DATA = "postData";
    public static final java.lang.String TRANSACTION_ID = "transactionID";
    public static final java.lang.String PAYMENT_ID = "paymentID";
    public static final java.lang.String DEFAULT_BASE_URL = "https://api.instamojo.com/";
    public static final int PENDING_PAYMENT = 2;

    public Constants() { /* compiled code */ }
}