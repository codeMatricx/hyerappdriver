package com.hyr_driver.utils;

public class Constant {

	public static final String FALSE = "false";
	public static final String TXT_BLANK = "";



	public static String UTF_8 = "UTF-8";
	public static String SERVER_RESPONSE_SUCCESS_CODE = "200";
	public static String CONTENT_TYPE = "Content-type";
	public static String APPLICATION_JSON = "application/json";
	public static String ACCESS = "Access";
	public static final String RESPONCE_CODE = "responseCode";
	public static final String RESPONCE_MESSAGE = "responseMessage";

	public static final String RESPONCECODE = "responsecode";
	public static final String RESPONCEMESSAGE = "responsemessage";

	/***  App Preference Keys */
	public static final String PREF_USERNAME 			= "fullname";
	public static final String PREF_USERID 				= "USER_ID";
	public static final String PREF_STSFF_USERID 		= "USER_ID";
	public static final String PREF_USER_IMAGE 			= "image";

	public static final String BASE_URL = "http://staging.isiwal.com/paycabsapi/driver/";

	public static final String LOGIN = "login.php";
	public static final String REGISTER = "register.php";
	public static final String UPDATE_PROFILE = "update.php";
	public static final String CURRENTLOCATION = "currentupdate.php";
	public static final String ARRIVED = "arrived.php";
	public static final String COMPLETE_RIDE = "complete_ride.php";
	public static final String OTP = "verify_driver.php";
	public static final String FORGETPASS = "forgotpassword.php";
	public static final String INSTANTBOOKING = "instant_booking.php";
	public static final String ACCEPT_REQUEST = "accept_request.php";
	public static final String REJECT_REQUEST = "request_reject.php";
	public static final String Rating = "ratings.php";
	public static final String Documents = "documents.php";
	public static final String StartTrips = "startTrip.php";
	public static final String MySchedulejob = "rideLaterList.php";
	public static final String ChangePassword = "changePassword.php";
	public static final String Earning ="earnings.php";
	public static final String Delete_Earning ="deleteEarning.php";

	public static class Config {
		public static final boolean DEVELOPER_MODE = false;
	}


}
