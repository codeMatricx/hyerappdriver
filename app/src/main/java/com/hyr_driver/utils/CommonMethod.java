package com.hyr_driver.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethod {

    private static Editor editor;
    public static String NOTIFICATION_STATUS="NOTIFICATION_STATUS";

    /**
     * Called for setting the value based on key in Prefs
     */

    public static void saveBookingidEarning(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Bookingidearning", value);
        editor.commit();
    }
    public static String getsaveBookingidEarning(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String PSource = sharedPreferences.getString("Bookingidearning", "");
        return PSource;
    }
 //==============================================================
    public static void savePassengerSource(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("PassengerSourec", value);
        editor.commit();
    }
    public static String getsavePassengerSource(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String PSource = sharedPreferences.getString("PassengerSourec", "");
        return PSource;
    }
 //======================================
 public static void saveBookingidAccept(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("AcceptBooking", value);
     editor.commit();
 }
    public static String getsaveBookingidAccept(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String ABooking = sharedPreferences.getString("AcceptBooking", "");
        return ABooking;
    }
//--------------------------
public static void saveOriginalFare(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("OriginalFare", value);
    editor.commit();
}
    public static String getsaveOriginalFare(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String Originalfare = sharedPreferences.getString("OriginalFare", "");
        return Originalfare;
    }
//-----------------------------------------
public static void savePassengerPhone(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("PassengerPhone", value);
    editor.commit();
}
    public static String getsavePassengerPhone(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String Pas_phone = sharedPreferences.getString("PassengerPhone", "");
        return Pas_phone;
    }
//===============================================
public static void savePassengerName(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("PassengerName", value);
    editor.commit();
}
    public static String getsavePassengerName(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String Devicetoken = sharedPreferences.getString("PassengerName", "");
        return Devicetoken;
    }
//================================================
public static void savePassengerDestination(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("PassengerDestination", value);
    editor.commit();
}
    public static String getsavePassengerDestination(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String Devicetoken = sharedPreferences.getString("PassengerDestination", "");
        return Devicetoken;
    }

//=======================================================
    public static void saveDeviceToken(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("DeviceToken", value);
        editor.commit();
    }
    public static String getsaveDeviceToken(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String Devicetoken = sharedPreferences.getString("DeviceToken", "");
        return Devicetoken;
    }
//----------------------------------------------------------
    public static void saveLoginPreferencesHospital(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("HOSPITAL", value);
        editor.commit();
    }

    public static String getLoginSavedPreferencesHospital(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String hospital_status = sharedPreferences.getString("HOSPITAL", "");
        return hospital_status;
    }

    public static String getLoginSavedPreferencesUSerID(Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_status = sharedPreferences.getString("USERID", "");
        return login_status;
    }


    public static void saveLoginPreferencesProductId(Context context, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("PRODUCTID", value);
        editor.commit();

    }


    public static String getLoginSavedPreferencesProductId(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String login_status = sharedPreferences.getString("PRODUCTID", "");

        return login_status;
    }






    public static void savepPin(Context context, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("PIN", value);
        editor.commit();

    }


    public static String getPin(Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        String login_status = sharedPreferences.getString("PIN", "");

        return login_status;
    }


    public static void saveUserId(String value, Context context)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("USERID", value);
        editor.commit();

    }
    public static String getSavedPreferencesUserId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_USERID = sharedPreferences.getString("USERID", "");
        return login_USERID;

    }

    public static void saveEmailid(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("EMAILID", value);
        editor.commit();

    }
    public static String getSavedPreferencesEmailid(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_EMAILID_ = sharedPreferences.getString("EMAILID", "");
        return login_EMAILID_;

    }
 //----------------------------------------------
 public static void savePassengerId(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("PassengerID", value);
     editor.commit();

 }
    public static String getsavePassengerId(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_PHONE = sharedPreferences.getString("PassengerID", "");
        return login_PHONE;

    }
 //--------------------------------------------------------
 public static void saveBookingIDMain(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("BookingIdMain", value);
     editor.commit();

 }
    public static String getSavedBookingIDMain(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Bookingidses = sharedPreferences.getString("BookingIdMain", "");
        return Bookingidses;

    }
 //-------------------------------------------------------
    public static void savePhone(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("PHONE", value);
        editor.commit();

    }
    public static String getSavedPreferencesPhone(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_PHONE = sharedPreferences.getString("PHONE", "");
        return login_PHONE;

    }
 //------------------------------------------------

    public static void saveDriverID(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Driver_ID", value);
        editor.commit();

    }
    public static String getSavedPreferencesDriverID(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String DRIVERID = sharedPreferences.getString("Driver_ID", "");
        return DRIVERID;

    }
//================================================================

    public static void saveNewSource(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("New_Source", value);
        editor.commit();

    }
    public static String getSavedPreferencesNewSource(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String NEWSOURCE = sharedPreferences.getString("New_Source", "");
        return NEWSOURCE;

    }
//===============================================================
public static void saveNewdESTINATION(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("New_Destination", value);
    editor.commit();

}
    public static String getSavedPreferencesNewDestination(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String NewDestination = sharedPreferences.getString("New_Destination", "");
        return NewDestination;

    }
//==========================================================

    public static void saveInSource(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("InSource", value);
        editor.commit();

    }
    public static String getSavedPreferencesInSource(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Insource = sharedPreferences.getString("InSource", "");
        return Insource;

    }
 //======================================================================
 public static void saveInDestination(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("InDestination", value);
     editor.commit();

 }
    public static String getSavedPreferencesInDestination(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Indestination = sharedPreferences.getString("InDestination", "");
        return Indestination;

    }
//================================================================

    public static void saveInFare(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("InFare", value);
        editor.commit();

    }
    public static String getSavedPreferencesInFare(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Infare = sharedPreferences.getString("InFare", "");
        return Infare;

    }
//====================================================================
public static void saveInTime(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("InTime", value);
    editor.commit();

}
    public static String getSavedPreferencesInTime(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Intime = sharedPreferences.getString("InTime", "");
        return Intime;

    }

//=============================================================

    public static void saveInPhonenumber(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("InPhonenumber", value);
        editor.commit();

    }
    public static String getSavedPreferencesInPhonenumber(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Inphno = sharedPreferences.getString("InPhonenumber", "");
        return Inphno;

    }
 //=============================================================

    public static void saveInRideType(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("InRidetype", value);
        editor.commit();

    }
    public static String getSavedPreferencesInRideType(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String InRideType = sharedPreferences.getString("InRidetype", "");
        return InRideType;

    }
 //==============================================================

    public static void saveInPaymentMode(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("InPaymentmode", value);
        editor.commit();

    }
    public static String getSavedPreferencesInPaymentMode(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String InPaymode = sharedPreferences.getString("InPaymentmode", "");
        return InPaymode;

    }

//=====================================================================

    public static void savePassword(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Driver_Password", value);
        editor.commit();

    }
    public static String getSavedPreferencesPassword(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String DRIVERPASS = sharedPreferences.getString("Driver_Password", "");
        return DRIVERPASS;

    }
//===============================================================================



//-----------------------------------------------

    public static void saveCabname(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("CABNAME", value);
        editor.commit();

    }
    public static String getSavedPreferencesCABname(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String cab_name = sharedPreferences.getString("CABNAME", "");
        return cab_name;

    }

 //--------------------------------------------------
 public static void saveRide_source(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("R_SOURCE", value);
     editor.commit();

 }
    public static String getSavedPreferencesRide_source(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_source = sharedPreferences.getString("R_SOURCE", "");
        return ride_source;

    }
 //------------------------------------------------------
 public static void saveRide_destination(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("RIDE_DESTINATION", value);
     editor.commit();

 }
    public static String getSavedPreferencesRide_destination(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_desti = sharedPreferences.getString("RIDE_DESTINATION", "");
        return ride_desti;

    }
 //------------------------------------------------------
 public static void saveRide_fare(Context context, String value)
 {
     SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
     Editor editor = sharedPreferences.edit();
     editor.putString("RIDE_FARE", value);
     editor.commit();

 }
    public static String getSavedPreferencesRide_fare(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_fare = sharedPreferences.getString("RIDE_FARE", "");
        return ride_fare;

    }
 //--------------------------------------------------------------------

    public static void saveRide_Source(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_SOURCE", value);
        editor.commit();

    }
    public static String getSavedPreferencesRide_Source(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_Sources = sharedPreferences.getString("RIDE_SOURCE", "");
        return ride_Sources;

    }
//----------------------------------------------------------

    public static void saveRide_Destinations(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_DESTINATIONS", value);
        editor.commit();

    }
    public static String getSavedPreferencesRide_Destinations(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_Destinationses = sharedPreferences.getString("RIDE_DESTINATIONS", "");
        return ride_Destinationses;

    }



    //------------------------------------------------
public static void saveRideSource(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("RIDE_SOURCE", value);
    editor.commit();

}
    public static String getSavedPreferencesRideSource(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_source = sharedPreferences.getString("RIDE_SOURCE", "");
        return ride_source;

    }

//--------------------------------------------------------------------

    public static void saveRideDestination(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("RIDE_DESTINATIONSES", value);
        editor.commit();

    }
    public static String getSavedPreferencesRideDestination(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_destinationes = sharedPreferences.getString("RIDE_DESTINATIONSES", "");
        return ride_destinationes;

    }



//-------------------------------------------------------
public static void saveRide_Time(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("RIDE_TIME", value);
    editor.commit();

}
    public static String getSavedPreferencesRide_Time(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String ride_times = sharedPreferences.getString("RIDE_TIME", "");
        return ride_times;

    }
//===============================================================
public static void saveRegisterphone(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("DriverPhone", value);
    editor.commit();

}
    public static String getSavedRegisterphone(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_USERID = sharedPreferences.getString("DriverPhone", "");
        return login_USERID;

    }
//---------------------------------------------------
    public static void saveUserName(Context context, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString("Username", value);
        editor.commit();

    }
    public static String getSavedPreferencesUserName(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String login_USERID = sharedPreferences.getString("Username", "");
        return login_USERID;

    }
//=============================================================
public static void saveInstantBookingID(Context context, String value)
{
    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    Editor editor = sharedPreferences.edit();
    editor.putString("Instantbooking", value);
    editor.commit();

}
    public static String getsaveInstantBookingID(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Instant_Booking = sharedPreferences.getString("Instantbooking", "");
        return Instant_Booking;

    }
//==========================================================
    /**
     * Called for getting the value based on key from Prefs
     */
    public static String getPrefsData(Context context, String prefsKey,
                                      String defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        String prefsValue = sharedPreferences.getString(prefsKey, defaultValue);
        return prefsValue;
    }

    /**
     * Called for Showing Alert in Application
     */
    public static void showAlert(String message, Activity context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        try {
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Called for checking Email Validation
     */
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * Called for checking Internet connection
     */
    public static boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo;
        try {
            netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return false;
    }




    public static String convertDate(String timestamp) {


        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm aa");

            Date resultdate = new Date(Long.valueOf(timestamp));
            System.out.println(dateFormat.format(resultdate));


            // Calendar calendar = Calendar.getInstance();
            //TimeZone tz = TimeZone.getDefault();
            // calendar.setTimeInMillis(Long.valueOf(timestamp) * 1000);
            // calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            // SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy-HH-mm-ss");
            //SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm aa");

            //Date currenTimeZone = (Date) calendar.getTime();


            return dateFormat.format(resultdate);
        } catch (Exception e) {
        }


        return "";
    }

    public static boolean copyStream(InputStream input, OutputStream output)
            throws IOException {
        boolean isCopied = false;
        byte[] buffer = new byte[1024];
        int bytesRead;
        try {
            while ((bytesRead = input.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
            isCopied = true;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            isCopied = false;
        }
        return isCopied;
    }

    public static void closeKeyboard(final Context context, final View view) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
