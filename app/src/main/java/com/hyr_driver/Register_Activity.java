package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.RegisterRequestparser;
import com.hyr_driver.responseParser.Register_responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;
import com.hyr_driver.utils.DbHelper;

import org.apache.http.entity.StringEntity;

public class Register_Activity extends AppCompatActivity {
Toolbar toolbartop;
    Button btn_register;
    EditText et_name,et_phone,et_email,et_pass,et_driving,et_city;
    String s_name,s_email,s_phone,s_pass,s_driving,s_city;
    private DbHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register_);

//        db = new DbHelper(this);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_register);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        et_name=(EditText)findViewById(R.id.edit_name);
        et_phone=(EditText)findViewById(R.id.edit_phone);
        et_email=(EditText)findViewById(R.id.edit_emial);
        et_pass=(EditText)findViewById(R.id.edit_pass);
        et_driving=(EditText)findViewById(R.id.edit_driving);
        et_city=(EditText)findViewById(R.id.edit_city);

        btn_register=(Button)findViewById(R.id.btn_regiser);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_name.getText().toString().length()==0){
                    et_name.setError("Please Enter Name");
                    et_name.requestFocus();
                }

                else if(et_email.getText().toString().length()==0){
                    et_email.setError("Please Enter Email Address");
                    et_email.requestFocus();
                }

                else if(et_phone.getText().toString().length()==0){
                    et_phone.setError("Please Enter Phone");
                    et_phone.requestFocus();
                }
                else if(et_pass.getText().toString().length()<=5){
                    et_pass.setError("Please Enter Password with minimum 6 digits");
                    et_pass.requestFocus();
                }
                else if(et_city.getText().toString().length()==0){
                    et_city.setError("Please Enter City");
                    et_city.requestFocus();
                }
                else if(et_driving.getText().toString().length()<=9){
                    et_driving.setError("Please Enter Driving Licence");
                    et_driving.requestFocus();
                }
                else {
               new SignupAsync().execute();
                }
                }

        });
    }

    private class SignupAsync extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Register_responseparser mRegister_responseparser;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Register_Activity.this, "", "Creating your profile...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in RegisterResponse Parser///////
                Gson gson = new Gson();
                mRegister_responseparser = gson.fromJson(response, Register_responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mRegister_responseparser != null) {
                if (mRegister_responseparser.code.trim().equals("200")) {
                    Intent intent = new Intent(Register_Activity.this, Activity_otp.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Register_Activity.this, mRegister_responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mRegister_responseparser.message.toString().trim(),
                            Register_Activity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Register_Activity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.REGISTER;
            Log.e("url----------------", "" + url);

            try {
//

                RegisterRequestparser mRegisterRequestparser = new RegisterRequestparser();
                s_name=mRegisterRequestparser.name = et_name.getText().toString().trim();
                s_phone=mRegisterRequestparser.phone = et_phone.getText().toString().trim();
                s_pass=mRegisterRequestparser.password = et_pass.getText().toString().trim();
                s_email = mRegisterRequestparser.email= et_email.getText().toString().trim();
                s_city=mRegisterRequestparser.city=et_city.getText().toString();
                s_driving=mRegisterRequestparser.drivingLicense=et_driving.getText().toString();

                CommonMethod.savePassword(getApplicationContext(),s_pass);
                CommonMethod.saveRegisterphone(getApplicationContext(),s_phone);
                Log.e("name----------------","" + s_name);
                Log.e("phone----------------","" + s_phone);


                Gson gson = new Gson();
                String requestInString = gson.toJson(mRegisterRequestparser, RegisterRequestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Register_Activity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;

        }

    }
}
