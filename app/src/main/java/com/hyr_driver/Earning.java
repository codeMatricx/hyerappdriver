package com.hyr_driver;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Earning extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    ListView simpleList;
    Toolbar toolbartop;
    String[] title = {"Bill Amount","hyr Commission","Ride Earning","Toll and Parking","Other Earning","Deductions"};
    String[] rupees = {"Rs. 19,92,128.00","Rs. 3,98,425.00","Rs. 3,54,350.00","Rs. 16,543.00","Rs. 3,358.00","Rs. 48,588.00"};
    String[] week = { "Select Week...","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday" };
    String[] cars = { "Select car type","Mini","Auto","Bike","Rental" };




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earning);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        simpleList = (ListView) findViewById(R.id.listView1);
        CustomAdapter customAdapter = new CustomAdapter();
        simpleList.setAdapter(customAdapter);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_fpass);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Spinner spin = (Spinner) findViewById(R.id.spn1);
        spin.setOnItemSelectedListener(this);
        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, week);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);

        Spinner spin1 = (Spinner) findViewById(R.id.spn2);
        spin1.setOnItemSelectedListener(this);
        ArrayAdapter a = new ArrayAdapter(this, android.R.layout.simple_spinner_item, cars);
        a.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin1.setAdapter(a);


    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Spinner spin = (Spinner)adapterView;
        Spinner spin1 = (Spinner)adapterView;
        if(spin.getId() == R.id.spn1)
        {
            Toast.makeText(this, "Week Day :" + week[i],Toast.LENGTH_SHORT).show();
        }
        if(spin1.getId() == R.id.spn2)
        {
            Toast.makeText(this, "Car :" + cars[i],Toast.LENGTH_SHORT).show();
        }

//
//
//        Toast.makeText(getApplicationContext(),month[i], Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(),cars[i],Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_earning, menu);
        return true;
    }

    public class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return title.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.earnlist,null);
            TextView detail = (TextView)view.findViewById(R.id.title);
            TextView detail1 = (TextView)view.findViewById(R.id.rupee);


            detail.setText(title[i]);
            detail1.setText(rupees[i]);

            return view;
        }

    }
}


