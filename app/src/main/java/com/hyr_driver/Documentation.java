package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.Accept_Requestparser;
import com.hyr_driver.requestParser.Documentation_Requestparser;
import com.hyr_driver.responseParser.Accept_Responseparser;
import com.hyr_driver.responseParser.Documentation_Responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;

public class Documentation extends Activity {
    EditText adhar,pan,vehical,rc,licen,vehiclename;
    Button submit;
    Toolbar toolbartop;
    String adhaar,pn,r_c,vehicl,licence,vehicl_names,driverides;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_documentation);

        toolbartop = (Toolbar) findViewById(R.id.toolbar_documentation);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        driverides=CommonMethod.getSavedPreferencesDriverID(Documentation.this);
        vehiclename=(EditText)findViewById(R.id.et_vehiclename);
        adhar=(EditText)findViewById(R.id.et_aadhr);
        pan=(EditText)findViewById(R.id.et_pan);
        rc=(EditText)findViewById(R.id.et_rc);
        vehical=(EditText)findViewById(R.id.et_vhclno);
        licen=(EditText)findViewById(R.id.et_licence) ;
        submit=(Button)findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(adhar.getText().toString().length()==0){
                    adhar.setError("Invalid Adhar Number");
                    adhar.requestFocus();
                }

                else if(pan.getText().toString().length()==0){
                    pan.setError("Invalid PanCard Number");
                    pan.requestFocus();
                }

                else if(rc.getText().toString().length()==0){
                    rc.setError("Invalid RC Number");
                    rc.requestFocus();
                }
                else if(vehical.getText().toString().length()<=9){
                    vehical.setError("Invalid Vehicle Number");
                    vehical.requestFocus();
                }
                else if(licen.getText().toString().length()==0){
                    licen.setError("Invalid Licence Number");
                    licen.requestFocus();
                }
                else if(vehiclename.getText().toString().length()==0){
                    vehiclename.setError("Invalid Vehicle Name");
                    vehiclename.requestFocus();
                }
                else {
                    adhaar = adhar.getText().toString();
                    pn = pan.getText().toString();
                    r_c = rc.getText().toString();
                    vehicl = vehical.getText().toString();
                    licence = licen.getText().toString();
                    vehicl_names=vehiclename.getText().toString();
                    new documents().execute();
                }
            }


        });
}

    private class documents extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Documentation_Responseparser documentation;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Documentation.this, "", "Submiting...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                documentation = gson.fromJson(response, Documentation_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (documentation != null) {
                if (documentation.code.trim().equals("200")) {
                    Intent intent = new Intent(Documentation.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Documentation.this, documentation.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(documentation.message.toString().trim(),
                            Documentation.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Documentation.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.Documents;
            Log.e("url----------------", "" + url);
            try {
                Documentation_Requestparser mDocumentation_Requestparser = new Documentation_Requestparser();
                String driverid=mDocumentation_Requestparser.panNumber=pn;
                String bookinid =mDocumentation_Requestparser.aadharcardNumber=adhaar;
                mDocumentation_Requestparser.userId=driverides;
                mDocumentation_Requestparser.vehicleName=vehicl_names;
                mDocumentation_Requestparser.vehicleNumber=vehicl;
                mDocumentation_Requestparser.drivingLicence=licence;
                mDocumentation_Requestparser.rcNumber=r_c;

                Log.e("Driverid----","" + driverid);
                Log.e("Bookingid---","" + bookinid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mDocumentation_Requestparser, Documentation_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Documentation.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}