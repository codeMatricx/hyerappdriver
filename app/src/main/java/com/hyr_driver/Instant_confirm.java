package com.hyr_driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.Extras.DirectionFinder;
import com.hyr_driver.Extras.DirectionFinderListener;
import com.hyr_driver.Extras.Route;
import com.hyr_driver.utils.CommonMethod;

import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Instant_confirm extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener,DirectionFinderListener {
    Toolbar toolbartop;
    TextView tv_source,tv_desti,tv_fare,tv_time,tv_paymode,tv_ridetype,tv_contact;
    String s_source,s_destination,s_fare,s_time,s_paymode,s_ridetype,s_phn;
    Button btn_confirm,btn_cancel;

    private GoogleMap mMap;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private static final String LOG_TAG = "PlaceSelectionListener";
    private ArrayList<LatLng> point; //added
    Polyline line; //added
    private int PROXIMITY_RADIUS = 10000;
    Context context;
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    String src_latlang,desti_latlang,address;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    double latitude;
    double longitude;

    String source,destination,fare,time,item,payment,abc,confirm_ridesource,confirm_ridedesti,st_bookid,str_bkingid,ridetype;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instant_confirm);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_instantconfirm);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_source=(TextView)findViewById(R.id.tb_source);
        tv_desti=(TextView)findViewById(R.id.tb_destination);
        tv_fare=(TextView)findViewById(R.id.tb_fare);
        tv_time=(TextView)findViewById(R.id.tb_time);
        tv_paymode=(TextView)findViewById(R.id.tb_paymentmode);
        tv_contact=(TextView)findViewById(R.id.tb_phonenumber);

        btn_confirm=(Button)findViewById(R.id.confirm_btn);
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (Instant_confirm.this,Instant_Starttrip.class);
                startActivity(i);

            }
        });
//
        source=CommonMethod.getSavedPreferencesInSource(Instant_confirm.this);
       destination=CommonMethod.getSavedPreferencesInDestination(Instant_confirm.this);
        s_source=CommonMethod.getSavedPreferencesRide_source(Instant_confirm.this);
        s_destination=CommonMethod.getSavedPreferencesRide_destination(Instant_confirm.this);
        s_fare=CommonMethod.getSavedPreferencesInFare(Instant_confirm.this);
        s_paymode=CommonMethod.getSavedPreferencesInPaymentMode(Instant_confirm.this);
        s_phn=CommonMethod.getSavedPreferencesInPhonenumber(Instant_confirm.this);
       s_time= CommonMethod.getSavedPreferencesInTime(Instant_confirm.this);
        s_ridetype=CommonMethod.getSavedPreferencesInRideType(Instant_confirm.this);

        tv_desti.setText(s_destination);
        tv_source.setText(s_source);
        tv_contact.setText(s_phn);
        tv_fare.setText(s_fare);
        tv_paymode.setText(s_paymode);
        //tv_ridetype.setText(s_ridetype);
        tv_time.setText(s_time);

        context = this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            finish();
        }
        else {
            Log.d("onCreate","Google Play Services available.");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Initializing
        mapFragment.getMapAsync(this);

        if(s_destination != null && s_source!=null)
        {
            sendRequest();
        }
    }

    //-----------------------------OnCreate Finshed------------------------//

    private void sendRequest() {
        String origin = s_source;
        String destinations = s_destination;
        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destinations.isEmpty()) {
            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            new DirectionFinder(this, origin, destinations).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng hcmus= new LatLng(28.579876,77.313748);
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hcmus, 18));
//        originMarkers.add(mMap.addMarker(new MarkerOptions()
//                .title(address)
//                .position(hcmus)));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);


//
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);





            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                tv_source.setText(address + " " + city + " " + country);
                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);


        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        Toast.makeText(Instant_confirm.this,"Your Current Location", Toast.LENGTH_LONG).show();

        // Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }






    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }





    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }


    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 11));
            //((TextView) findViewById(R.id.tv_etime)).setText(route.duration.text);
            //((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLACK).
                    width(6);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


//    private class Confirmride extends AsyncTask<String, Void, String> {
//        private ProgressDialog progress;
//        private ConfirmRide_Responseparser mConfirmRide_Responseparser;
//        private String response = "";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progress = ProgressDialog.show(Confimation.this, "", "Finding Driver...", true);
//            progress.setCancelable(true);
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            response = CallConfirmationService();
//            return response;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            progress.dismiss();
//
//            try {
//                // get response from server side and store in SignupResponse Parser///////
//                Gson gson = new Gson();
//                mConfirmRide_Responseparser = gson.fromJson(response, ConfirmRide_Responseparser.class);
//            } catch (JsonSyntaxException e) {
//                e.printStackTrace();
//            } catch (JsonIOException e) {
//                e.printStackTrace();
//            }
//            if (mConfirmRide_Responseparser != null) {
//                if (mConfirmRide_Responseparser.code.trim().equals("200")) {
//
//                    str_bookingid=mConfirmRide_Responseparser.request.bookingId;
//                    str_driverid=  mConfirmRide_Responseparser.request.driverId;
//                    str_pasngrid= mConfirmRide_Responseparser.request.passengerId;
//                    str_source= mConfirmRide_Responseparser.request.source;
//                    str_destination=mConfirmRide_Responseparser.request.destination;
//                    str_drivername=mConfirmRide_Responseparser.request.driverName;
//                    str_drivernumber=mConfirmRide_Responseparser.request.driverPhone;
//                    str_pasngrname=mConfirmRide_Responseparser.request.passengerName;
//                    str_pasngrnumber=mConfirmRide_Responseparser.request.passengerPhone;
//                    str_vehiclename=mConfirmRide_Responseparser.request.vehicleName;
//                    str_vehiclenumber=mConfirmRide_Responseparser.request.vehicleNumber;
//
//                    CommonMethod.saveDriverName(getApplicationContext(),str_drivername);
//                    CommonMethod.saveDriverNumber(getApplicationContext(),str_drivernumber);
//                    CommonMethod.saveVehicleName(getApplicationContext(),str_vehiclename);
//                    CommonMethod.saveVehicleNumber(getApplicationContext(),str_vehiclenumber);
//
//                    Intent intent = new Intent(Confimation.this, Driver_in_Route.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
//                    Toast.makeText(Confimation.this, mConfirmRide_Responseparser.message, Toast.LENGTH_SHORT).show();
//
//                } else {
//                    CommonMethod.showAlert(mConfirmRide_Responseparser.message.toString().trim(),
//                            Confimation.this);
//                }
//            } else {
//                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Confimation.this);
//            }
//        }
//
//        @SuppressLint("DefaultLocale")
//
//        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////
//
//        private String CallConfirmationService() {
//            String url = "http://staging.isiwal.com/paycabsapi/passenger/send_request.php";
//            Log.e("url----------------", "" + url);
//            try {
//                ConfirmRide_Requestparser mConfirmRide_Requestparser = new ConfirmRide_Requestparser();
//
//                String a=mConfirmRide_Requestparser.passengerId=str_id;
//                String b=mConfirmRide_Requestparser.bookingId=str_bkingid;
//                String c=mConfirmRide_Requestparser.driverId=str_id;
//
//
//
//
//                Log.e("PID----------------","" + a);
//                Log.e("BID----------------","" + b);
//                Log.e("DID----------------","" + c);
//
//
//                Gson gson = new Gson();
//                String requestInString = gson.toJson(mConfirmRide_Requestparser, ConfirmRide_Requestparser.class);
//                System.out.println(requestInString);
//                StringEntity stringEntity = new StringEntity(requestInString);
//                ServiceCall serviceCall = new ServiceCall(Confimation.this, url, stringEntity);
//                response = serviceCall.getServiceResponse();
//                Log.e("Response -- ", "" + response);
//                System.out.println(serviceCall.getServiceResponse());
//
//
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return response;
//        }
//    }
//}


}
