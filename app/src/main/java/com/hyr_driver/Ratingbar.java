package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.OtpRequestParser;
import com.hyr_driver.requestParser.Rating_Requestparser;
import com.hyr_driver.responseParser.OtpResponseParser;
import com.hyr_driver.responseParser.Rating_Responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;

public class Ratingbar extends AppCompatActivity {
Toolbar toolbartop;
    Button btn_rating;
    RatingBar rating;
    TextView tv_source,tv_destination,fare;
    String source,destination,driverid,ratings,pasnid,originalfares;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_ratingbar);

        tv_source=(TextView)findViewById(R.id.tv_rating_source);
        fare=(TextView)findViewById(R.id.fare_final);
        tv_destination=(TextView)findViewById(R.id.tv_rating_destination);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_ratingbars);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        addListenerOnButtonClick();
        pasnid=CommonMethod.getsavePassengerId(Ratingbar.this);
        originalfares=CommonMethod.getsaveOriginalFare(Ratingbar.this);
        driverid=CommonMethod.getSavedPreferencesDriverID(Ratingbar.this);
        source= CommonMethod.getSavedPreferencesRide_source(Ratingbar.this);
        destination =CommonMethod.getSavedPreferencesRide_destination(Ratingbar.this);
        tv_source.setText(source);
        tv_destination.setText(destination);
        fare.setText(originalfares);

    }

    private void addListenerOnButtonClick() {
        rating=(RatingBar)findViewById(R.id.ratingBar1);
        btn_rating=(Button)findViewById(R.id.rating_submit_btn);
        btn_rating.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                ratings=String.valueOf(rating.getRating());
                if (ratings!=""){
                    new Ratings().execute();
                    Toast.makeText(Ratingbar.this, ""+ratings, Toast.LENGTH_SHORT).show();
                }


            }

        });
    }

    private class Ratings extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Rating_Responseparser mRating_Responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Ratingbar.this, "", "Thanks Please wait...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mRating_Responseparser = gson.fromJson(response, Rating_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mRating_Responseparser != null) {
                if (mRating_Responseparser.code.trim().equals("200")) {
                    Intent intent = new Intent(Ratingbar.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Ratingbar.this, mRating_Responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mRating_Responseparser.message.toString().trim(),
                            Ratingbar.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Ratingbar.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.Rating;
            Log.e("url----------------", "" + url);
            try {
                Rating_Requestparser mRating_Requestparser = new Rating_Requestparser();
                String driverides=mRating_Requestparser.driverId = driverid;
                String pasn_id =mRating_Requestparser.passengerId= pasnid;
                String ratinges= mRating_Requestparser.rating= ratings;

                Log.e("Driverid----","" + driverides);
                Log.e("Pasengerid-----","" + pasn_id);
                Log.e("Ratings----","" + ratinges);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mRating_Requestparser, Rating_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Ratingbar.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
