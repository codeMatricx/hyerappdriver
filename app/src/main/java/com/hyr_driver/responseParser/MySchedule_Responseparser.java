package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 1/25/2018.
 */

public class MySchedule_Responseparser {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("rideLaterList")
    @Expose
    public List<RideLaterList> rideLaterList;
}
