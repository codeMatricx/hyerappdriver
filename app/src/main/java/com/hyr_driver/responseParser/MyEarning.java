package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/27/2018.
 */

public class MyEarning {
    @SerializedName("dayEarning")
    @Expose
    private String dayEarning;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("ride_date")
    @Expose
    public String rideDate;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;

    public String getDayEarning() {
        return dayEarning;
    }

    public void setDayEarning(String dayEarning) {
        this.dayEarning = dayEarning;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getRideDate() {
        return rideDate;
    }

    public void setRideDate(String rideDate) {
        this.rideDate = rideDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
