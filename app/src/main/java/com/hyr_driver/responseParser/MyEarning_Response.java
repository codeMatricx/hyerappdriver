package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iSiwal on 1/27/2018.
 */

public class MyEarning_Response {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("total_earning")
    @Expose
    public String totalEarning;
    @SerializedName("myEarning")
    @Expose
    public List<MyEarning> myEarning;
}
