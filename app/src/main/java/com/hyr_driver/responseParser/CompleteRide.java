package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/16/2018.
 */

public class CompleteRide {
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("ride_type")
    @Expose
    public String rideType;
    @SerializedName("vehicle_type")
    @Expose
    public String vehicleType;
    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;
    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("driver_name")
    @Expose
    public String driverName;
    @SerializedName("driver_phone")
    @Expose
    public String driverPhone;
    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
    @SerializedName("passenger_name")
    @Expose
    public String passengerName;
    @SerializedName("number_passenger")
    @Expose
    public String numberPassenger;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("fare")
    @Expose
    public String fare;
    @SerializedName("ride_date")
    @Expose
    public String rideDate;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("booking_date_time")
    @Expose
    public String bookingDateTime;
    @SerializedName("trip_complete_status")
    @Expose
    public String tripCompleteStatus;
    @SerializedName("originalFare")
    @Expose
    public String originalFare;
    @SerializedName("adminCommission")
    @Expose
    public String adminCommission;
    @SerializedName("gstTax")
    @Expose
    public String gstTax;
    @SerializedName("otherTax")
    @Expose
    public String otherTax;
}
