package com.hyr_driver.responseParser;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/25/2017.
 */

public class Currentlocation_Response {
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("Location")
    @Expose
    public Location location;
}
