package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/15/2018.
 */

public class Details {
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("aadharcard_number")
    @Expose
    public String aadharcardNumber;
    @SerializedName("pan_number")
    @Expose
    public String panNumber;
    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;
    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("rcNumber")
    @Expose
    public String rcNumber;
    @SerializedName("drivingLicence")
    @Expose
    public String drivingLicence;
}
