package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/25/2017.
 */

public class Location {

    @SerializedName("passenger_name")
    @Expose
    public String passengerName;
    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
    @SerializedName("driver_name")
    @Expose
    public String driverName;
    @SerializedName("driver_phone")
    @Expose
    public String driverPhone;
    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;
    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("is_trip_complete")
    @Expose
    public String isTripComplete;
    @SerializedName("driverBooked")
    @Expose
    public String driverBooked;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("passengerSource")
    @Expose
    public String passengerSource;
    @SerializedName("passengerDestination")
    @Expose
    public String passengerDestination;
    
    }
