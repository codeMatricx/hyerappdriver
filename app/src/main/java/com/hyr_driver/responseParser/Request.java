package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 1/13/2018.
 */

public class Request {

    @SerializedName("driver_lat")
    @Expose
    public String driverLat;
    @SerializedName("driver_long")
    @Expose
    public String driverLong;
    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("passenger_id")
    @Expose
    public String passengerId;
    @SerializedName("driver_id")
    @Expose
    public String driverId;
    @SerializedName("driver_name")
    @Expose
    public String driverName;
    @SerializedName("driver_phone")
    @Expose
    public String driverPhone;
    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;
    @SerializedName("vehicle_number")
    @Expose
    public String vehicleNumber;
    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
}