package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/22/2017.
 */

public class CabArrival {

    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
    @SerializedName("passenger_name")
    @Expose
    public String passengerName;
    @SerializedName("driver_name")
    @Expose
    public String driverName;
    @SerializedName("driver_phone")
    @Expose
    public String driverPhone;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
}
