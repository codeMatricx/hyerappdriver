package com.hyr_driver.responseParser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by iSiwal on 11/28/2017.
 */

public class Home {

    @SerializedName("booking_id")
    @Expose
    public String bookingId;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("destination")
    @Expose
    public String destination;
    @SerializedName("fare")
    @Expose
    public String fare;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("ride_type")
    @Expose
    public String rideType;
    @SerializedName("passenger_phone")
    @Expose
    public String passengerPhone;
    @SerializedName("payment_mode")
    @Expose
    public String paymentMode;
}
