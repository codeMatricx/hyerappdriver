package com.hyr_driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.Extras.Constants;
import com.hyr_driver.Extras.DataParse;
import com.hyr_driver.Extras.DirectionFinderListener;
import com.hyr_driver.Extras.Route;
import com.hyr_driver.requestParser.Arrived_Requestparser;
import com.hyr_driver.requestParser.CompleteRide_Requestparser;
import com.hyr_driver.requestParser.RegisterRequestparser;
import com.hyr_driver.requestParser.UpdateRequestparser;
import com.hyr_driver.responseParser.Arrived_Responseparser;
import com.hyr_driver.responseParser.CompleteRide_Responseparser;
import com.hyr_driver.responseParser.Register_responseparser;
import com.hyr_driver.responseParser.UpdateResponseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class After_Accept extends AppCompatActivity implements OnMapReadyCallback, PlaceSelectionListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, AdapterView.OnItemSelectedListener,DirectionFinderListener {

    private GoogleMap mMap;
    ArrayList<LatLng> MarkerPoints;
    Context context;
    PopupWindow popupWindow;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private static final String LOG_TAG = "PlaceSelectionListener";
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    public GoogleMap mGoogleMap;
    String arrived_userid,arrived_bookingid,ad_DriverName,ad_DriverPhone;
    private LatLng mLatLng;
    private double longitude1;
    double latitude;
    double longitude;
    private double latitude1;
    private TextView address_textView;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Boolean isInternetPresent = false;
    Button btn_call,btn_arriveds,btn_complete;
    FrameLayout container;
    Toolbar toolbartop;
    String src_latlang,af_pas_phones,address,driver_ids,booking_ids,pas_af_source,pas_af_name;
    TextView pasName,pasSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_after__accept);
        toolbartop = (Toolbar) findViewById(R.id.confirmation_toolbar);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        pasName=(TextView)findViewById(R.id.tv_af_pasname);
        pasSource=(TextView)findViewById(R.id.tv_af_source);

        btn_arriveds=(Button)findViewById(R.id.btn_arrived);
        driver_ids=CommonMethod.getSavedPreferencesDriverID(After_Accept.this);
        booking_ids=CommonMethod.getSavedBookingIDMain(After_Accept.this);
        af_pas_phones=CommonMethod.getsavePassengerPhone(After_Accept.this);
        pas_af_source=CommonMethod.getsavePassengerSource(After_Accept.this);
        pas_af_name=CommonMethod.getsavePassengerName(After_Accept.this);
        pasSource.setText(pas_af_source);
        pasName.setText(pas_af_name);
        btn_arriveds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Syncarrived().execute();

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Initializing
        MarkerPoints = new ArrayList<>();
        container = (FrameLayout) findViewById(R.id.map);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();

            getUserLocation();
            initMap();
            if (mGoogleApiClient.isConnected() && mLastLocation != null) {
                startIntentService();
            }
            getGPSInfo();
        } else {
        }


        btn_call = (Button) findViewById(R.id.call_pasenger);
        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + af_pas_phones));
                if (ActivityCompat.checkSelfPermission(After_Accept.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
            }
        });


        }




private void getGPSInfo() {
        Criteria criteria = new Criteria();
        String provider;
        Location location;
        LocationManager locationmanager = (LocationManager) After_Accept.this
        .getSystemService(Context.LOCATION_SERVICE);


        if (locationmanager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
        provider = locationmanager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(After_Accept.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(After_Accept.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
        return;
        }
        location = locationmanager.getLastKnownLocation(provider);

        } else {
        showGPSDisabledAlertToUser();
        }


        }

private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(After_Accept.this);
        alertDialogBuilder
        .setMessage(
        "GPS is disabled in your device. Would you like to enable it?")
        .setCancelable(false)
        .setPositiveButton("Goto Settings To Enable GPS",
        new DialogInterface.OnClickListener() {
public void onClick(DialogInterface dialog, int id) {
        Intent callGPSSettingIntent = new Intent(
        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(callGPSSettingIntent);

        }
        });

        alertDialogBuilder.setNegativeButton("Cancel",
        new DialogInterface.OnClickListener() {
public void onClick(DialogInterface dialog, int id) {
        finish();
        }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();


        }

private void startIntentService() {
        Intent intent = new Intent(this, After_Accept.class);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);
        startService(intent);
        }

private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getUserLocation();
        }

//Getting current location
private void getUserLocation() {
        //  mGoogleMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        // TODO: Consider calling
        //    ActivityCompat#requestPermissions
        // here to request the missing permissions, and then overriding
        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
        //                                          int[] grantResults)
        // to handle the case where the user grants the permission. See the documentation
        // for ActivityCompat#requestPermissions for more details.
        return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
        //Getting longitude and latitude
        longitude1 = location.getLongitude();
        latitude1 = location.getLatitude();

        //moving the map to location
        moveMap();
        }
        }

//Function to move the map
public void moveMap() {
    //String to display current latitude and longitude
    String locationmsg = longitude1 + ", " + latitude1;

    //Creating a LatLng Object to store Coordinates
    LatLng slatLng = new LatLng(latitude1, longitude1);

    //Adding marker to map
    mGoogleMap.addMarker(new MarkerOptions()
            .position(slatLng) //setting position
            .draggable(true) //Making the marker draggable
            .title("Current Location")); //Adding a title

    //Moving the camera
    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(slatLng));

    //Animating the camera
    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));

}


protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        }

protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        }

protected void onResume() {
        super.onResume();


        }


public boolean googleServiceAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
        return true;
        } else if (api.isUserResolvableError(isAvailable)) {
        Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
        dialog.show();
        } else {
        Toast.makeText(After_Accept.this, "can not connect play services", Toast.LENGTH_LONG).show();
        }
        return false;


        }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng hcmus= new LatLng(28.579876,77.313748);
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hcmus, 7));
//        originMarkers.add(mMap.addMarker(new MarkerOptions()
//                .title(address)
//                .position(hcmus)));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);


//
    }


@Override
public void onPlaceSelected(Place place) {

        }

    @Override
    public void onError(Status status) {

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        
    }

    @Override
public void onNothingSelected(AdapterView<?> parent) {

        }



    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);





            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //tv_source.setText(address + " " + city + " " + country);
                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);


        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        Toast.makeText(After_Accept.this,"Your Current Location", Toast.LENGTH_LONG).show();

        // Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }






    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }





    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 11));
           // ((TextView) findViewById(R.id.tv_etime)).setText(route.duration.text);
            //((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLACK).
                    width(6);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    private class Syncarrived extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Arrived_Responseparser mArrived_Responseparser;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(After_Accept.this, "", "Arrived...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in ArrivedResponse Parser///////
                Gson gson = new Gson();
                mArrived_Responseparser = gson.fromJson(response, Arrived_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mArrived_Responseparser != null) {
                if (mArrived_Responseparser.code.trim().equals("200")) {
                    ad_DriverName=mArrived_Responseparser.cabArrival.driverName;
                   ad_DriverPhone= mArrived_Responseparser.cabArrival.driverPhone;


                    Intent intent = new Intent(After_Accept.this, After_Arrived.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    //Toast.makeText(After_Accept.this, mArrived_Responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mArrived_Responseparser.message.toString().trim(),
                            After_Accept.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), After_Accept.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.ARRIVED;
            Log.e("url----------------", "" + url);

            try {

                Arrived_Requestparser mArrived_Requestparser = new Arrived_Requestparser();
                arrived_userid= mArrived_Requestparser.userId =driver_ids;
                arrived_bookingid= mArrived_Requestparser.bookingId = booking_ids;

                Log.e("A_userid-----------","" + arrived_userid);
                Log.e("A_bookingid---------","" + arrived_bookingid);


                Gson gson = new Gson();
                String requestInString = gson.toJson(mArrived_Requestparser, Arrived_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(After_Accept.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;

        }

    }


}

