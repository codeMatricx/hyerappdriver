package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyr_driver.responseParser.RideLaterList;

import java.util.List;


/**
 * Created by iSiwal on 1/25/2018.
 */

public class MySchedulejob_Adapter extends BaseAdapter {
    Context context;
    List<RideLaterList> list;


    public MySchedulejob_Adapter(Context context, List<RideLaterList> list)
    {
        this.context = context;
        this.list=list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        MySchedulejob_Adapter.Holder holder = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.myschedule_job, null);
            holder = new MySchedulejob_Adapter.Holder();
            holder.time=(TextView)convertView.findViewById(R.id.tv_time);
            holder.Date=(TextView)convertView.findViewById(R.id.tv_date);
            holder.source=(TextView)convertView.findViewById(R.id.tv_source);
            holder.destination=(TextView)convertView.findViewById(R.id.tv_destination);
            holder.images_delete=(ImageView)convertView.findViewById(R.id.img_delete);
            holder.images_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer index = (Integer) v.getTag();
                    //items.remove(index.intValue());
                    list.remove(position);
                    notifyDataSetChanged();

                }
            });

            convertView.setTag(holder);
        }
        else
        {
            holder = (MySchedulejob_Adapter.Holder) convertView.getTag();
        }

        holder.time.setText(list.get(position).bookingTime);
        holder.Date.setText(list.get(position).bookingDate);
        holder.source.setText(list.get(position).source);
        holder.destination.setText(list.get(position).destination);


        return convertView;

    }

    public class Holder {
        TextView time,Date,source,destination;
        ImageView images_delete;


    }


}
