package com.hyr_driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.Extras.SessionManager;
import com.hyr_driver.requestParser.Accept_Requestparser;
import com.hyr_driver.requestParser.Currentlocation_Request;
import com.hyr_driver.requestParser.Reject_RequestParser;
import com.hyr_driver.responseParser.Accept_Responseparser;
import com.hyr_driver.responseParser.Currentlocation_Response;
import com.hyr_driver.responseParser.Reject_Responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;
import com.hyr_driver.utils.Session;

import org.apache.http.entity.StringEntity;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;




public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    boolean flag=false;
    double latitude;
    double longitude;
    Handler ha=new Handler();
    Timer t = new Timer();
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    String string_driverid,str_lat,str_long,str_driverid,str_driveriduser,driverids,AcceptBooking;
    Button btn_accept,btn_reject;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int DPLACE_PICKER_REQUEST = 2;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1;
    private static String TAG = "MAP LOCATION";
    Context mContext;
    TextView tv_name,tv_email,tv_phone;
    ImageView img_edit;
    Ringtone r;
    protected LocationManager locationManager;
    Location location;
    double lng;
    SessionManager sessionmanager;
    RelativeLayout linear;
    Switch defaultSwitch;
    String passengerides,driverides,bookingidess,address,address_destination,pas_source,pas_destination,pas_names;
    DrawerLayout mDrawerLayout;
    NavigationView navigationView;
    TextView tv_source,tv_desti,pasenger_source,pasenger_destination,pasenger_name;
    Toolbar toolbar;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    Toolbar mToolbar;
    String Common_name,Common_email,pas_phones,Common_phone,cab_namees,src_latlang,desti_latlang,booking_id,passngr_id,istripcomplete,driver_bookedes;
    SessionManager session;
    Session s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        driverides=CommonMethod.getSavedPreferencesDriverID(MainActivity.this);
        bookingidess=CommonMethod.getSavedBookingIDMain(MainActivity.this);
        passengerides=CommonMethod.getsavePassengerId(MainActivity.this);


        //Toast.makeText(MainActivity.this, ""+bookingidess, Toast.LENGTH_SHORT).show();
        Log.e("AcceptBooking",""+AcceptBooking);

        mContext = this;
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        s=new Session(this);
        session = new SessionManager(getApplicationContext());


        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            finish();
        }
        else {
            Log.d("onCreate","Google Play Services available.");
        }
        defaultSwitch=(Switch)findViewById(R.id.default_switch);
        linear=(RelativeLayout) findViewById(R.id.linear_notification);
        //Popup Message
        pasenger_source=(TextView)findViewById(R.id.tv_pas_source);
        pasenger_destination=(TextView)findViewById(R.id.tv_pas_destination);
        pasenger_name=(TextView)findViewById(R.id.tv_pas_name);
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();

        defaultSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
             //Toast.makeText(MainActivity.this, "The Switch is " + (isChecked ? "1" : "0"), Toast.LENGTH_SHORT).show();

                if(isChecked) {
                    s.setSwitch(true);
                    ha.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            //call function
                            new Currentlocation().execute();
                            ha.postDelayed(this, 20000);
                        }
                    }, 20000);


                }
                else {
                    //r.stop();
                    ha.removeCallbacksAndMessages(null);
                    s.setSwitch(false);
                    linear.setVisibility(View.INVISIBLE);

                }
            }
        });
        driverids= CommonMethod.getSavedPreferencesDriverID(MainActivity.this);
        btn_accept=(Button)findViewById(R.id.accept_pasengr);
        btn_reject=(Button)findViewById(R.id.reject_pasengr);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r.stop();
                new jobaccept().execute();
            }
        });
        btn_reject.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setMessage("Are you sure want to Reject this Pickup");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        r.stop();
                        linear.setVisibility(View.INVISIBLE);
                        new Rejectrequest().execute();
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

});
        //mLocationText = (TextView) findViewById(R.id.Locality);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Common_name= CommonMethod.getSavedPreferencesUserName(MainActivity.this);
        Common_phone= CommonMethod.getSavedPreferencesPhone(MainActivity.this);
        Common_email= CommonMethod.getSavedPreferencesEmailid(MainActivity.this);
        cab_namees= CommonMethod.getSavedPreferencesCABname(MainActivity.this);

        mapFragment.getMapAsync(this);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

         navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        tv_name=(TextView)header.findViewById(R.id.nav_drawer_nametv);
        tv_email=(TextView)header.findViewById(R.id.nav_drawer_tvEmail);
        tv_phone=(TextView)header.findViewById(R.id.nav_drawer_tvMobile);
        img_edit=(ImageView)header.findViewById(R.id.edit_profile);
        img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,Edit_Profile.class);
                startActivity(i);
            }
        });
        tv_name.setText(Common_name);
        tv_phone.setText(Common_phone);
        tv_email.setText(Common_email);
    }



    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }



    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

              //  tv_source.setText(address + " " + city + " " + country);

                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(address);


        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        Toast.makeText(MainActivity.this,"Your Current Location", Toast.LENGTH_LONG).show();

        Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
              address =String.format("%s", place.getAddress());
//                stBuilder.append("Name: ");
               // stBuilder.append(placename);
               /* stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);*/
                // stBuilder.append("\n");
//                stBuilder.append("Address: ");
                stBuilder.append(address);
                tv_source.setText(stBuilder.toString());
                 src_latlang=(latitude+","+longitude);
            }
        }

        if (requestCode == DPLACE_PICKER_REQUEST) {
            Place place = PlacePicker.getPlace(data, this);
            StringBuilder stBuilder = new StringBuilder();
            //String placename = String.format("%s", place.getName());
            String latitudes = String.valueOf(place.getLatLng().latitude);
            String longitudes = String.valueOf(place.getLatLng().longitude);
            address_destination = String.format("%s", place.getAddress());
           // stBuilder.append("Name: ");
          //  stBuilder.append(placename);

           // stBuilder.append("\n");
            //stBuilder.append("Address: ");
            stBuilder.append(address_destination);
            tv_desti.setText(stBuilder.toString());
            desti_latlang=(latitudes+","+longitudes);

        }
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_instant) {
            Intent k = new Intent(MainActivity.this, Instant.class);
            startActivity(k);
        }
//        else if (id == R.id.nav_yourrides) {
//            Intent k = new Intent(MainActivity.this, Documentation.class);
//            startActivity(k);

        else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent
                    .putExtra(Intent.EXTRA_TEXT,
                            "I am using "
                                    + getString(R.string.app_name)
                                    + " App ! Why don't you try it out...\nInstall "
                                    + getString(R.string.app_name)
                                    + " now !\nhttps://play.google.com/store/apps/details?id="
                                    + getPackageName());

            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name) + " App !");

            sendIntent.setType("text/plain");


            startActivity(Intent.createChooser(sendIntent, getString(R.string.Share_App)));
            return true;


        } else if (id == R.id.nav_home) {

        }  else if (id == R.id.nav_docu) {
            Intent k = new Intent(MainActivity.this,Documentation.class);
            startActivity(k);
        } else if (id == R.id.nav_earning) {
            Intent n = new Intent(MainActivity.this,Earning_Driver.class);
            startActivity(n);
        } else if (id == R.id.nav_logout) {
              openExitDialog();
        }else if (id == R.id.nav_scheduleride) {
            Intent n = new Intent(MainActivity.this,MyScheduleRides.class);
            startActivity(n);
        }  else if (id == R.id.nav_privacy) {
            Intent n = new Intent(MainActivity.this,Privacypolicy.class);
            startActivity(n);
        }
        else if (id == R.id.nav_changepass) {
            Intent n = new Intent(MainActivity.this,ChangePassword.class);
            startActivity(n);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }
    public void openExitDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure want to logout ?");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        s.setLoggedin(false);
                        startActivity(new Intent(MainActivity.this,After_Splash.class));
                        finish();
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private class Currentlocation extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Currentlocation_Response mCurrentlocation_Response;
        private String response = "";
        boolean valid = true;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//               progress = ProgressDialog.show(MainActivity.this, "", "Duty is on...", true);
//               progress.setCancelable(true);

        }
        @Override
        protected String doInBackground(String... params) {
            response = CallSignupService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
           // progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mCurrentlocation_Response = gson.fromJson(response, Currentlocation_Response.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mCurrentlocation_Response != null) {
                if (mCurrentlocation_Response.code.trim().equals("200")) {
                    string_driverid=mCurrentlocation_Response.location.driverId;
                   booking_id= mCurrentlocation_Response.location.bookingId;
                    driver_bookedes= mCurrentlocation_Response.location.driverBooked;
                    istripcomplete=mCurrentlocation_Response.location.isTripComplete;
                    passngr_id=mCurrentlocation_Response.location.passengerId;
                    pas_destination=mCurrentlocation_Response.location.passengerDestination;
                    pas_source=mCurrentlocation_Response.location.passengerSource;
                    pas_names=mCurrentlocation_Response.location.passengerName;
                    pas_phones=mCurrentlocation_Response.location.passengerPhone;


                    CommonMethod.savePassengerSource(getApplicationContext(),pas_source);
                    CommonMethod.savePassengerDestination(getApplicationContext(),pas_destination);
                    CommonMethod.saveBookingIDMain(getApplicationContext(),booking_id);
                    CommonMethod.savePassengerId(getApplicationContext(),passngr_id);
                    CommonMethod.savePassengerName(getApplicationContext(),pas_names);
                    CommonMethod.savePassengerPhone(getApplicationContext(),pas_phones);
                    CommonMethod.saveBookingidAccept(getApplicationContext(),booking_id);

                    if(driver_bookedes.equals("1")){
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                        r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();
                        linear.setVisibility(View.VISIBLE);
                    }
                    else if(driver_bookedes.equals("2")){
                        linear.setVisibility(View.INVISIBLE);
                    }

                    Log.e("DD_Ides----------------", "" + string_driverid);
                    Log.e("Booking_ides------", "" + booking_id);
                    pasenger_source.setText(pas_source);
                    pasenger_destination.setText(pas_destination);
                    pasenger_name.setText(pas_names);


                    //Toast.makeText(MainActivity.this, mCurrentlocation_Response.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mCurrentlocation_Response.message.toString().trim(),MainActivity.this);
                }
            } else {
//                r.stop();
                //CommonMethod.showAlert(getResources().getString(R.string.connection_error), MainActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallSignupService() {
            String url = Constant.BASE_URL + Constant.CURRENTLOCATION;
            Log.e("url----------------", "" + url);

            try {
                Currentlocation_Request mCurrentlocation_Request = new Currentlocation_Request();
                str_lat=mCurrentlocation_Request.lat = String.valueOf(latitude);
                str_long=mCurrentlocation_Request.lng = String.valueOf(longitude);
                str_driveriduser=mCurrentlocation_Request.userId=driverids;
                mCurrentlocation_Request.isdutyon="1";


                Log.e("Latitude-----------","" + str_lat);
                Log.e("Longitude---------","" + str_long);
                Log.e("Driverid---------","" + str_driveriduser);


                Gson gson = new Gson();
                String requestInString = gson.toJson(mCurrentlocation_Request, Currentlocation_Request.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MainActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());




            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;

        }

    }

    //data started here


    @Override
    protected void onDestroy() {
        super.onDestroy();
        s.setSwitch(false);
        s.cleared();


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (s.checkedin())
        {
            defaultSwitch.setChecked(true);
        }
    }

    private class jobaccept extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Accept_Responseparser mAccept_Responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MainActivity.this, "", "Accepting...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mAccept_Responseparser = gson.fromJson(response, Accept_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mAccept_Responseparser != null) {
                if (mAccept_Responseparser.code.trim().equals("200")) {

                    Intent intent = new Intent(MainActivity.this, After_Accept.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    Toast.makeText(MainActivity.this, mAccept_Responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mAccept_Responseparser.message.toString().trim(),
                            MainActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MainActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.ACCEPT_REQUEST;
            Log.e("url----------------", "" + url);
            try {
                AcceptBooking=CommonMethod.getsaveBookingidAccept(MainActivity.this);
                Accept_Requestparser mAccept_Requestparser = new Accept_Requestparser();
                String driverid=mAccept_Requestparser.driverId=driverides;
                String bookinid =mAccept_Requestparser.bookingId=AcceptBooking;
//
//
                Log.e("Driverid----","" + driverid);
                Log.e("Bookingid---","" + bookinid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mAccept_Requestparser, Accept_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MainActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    private class Rejectrequest extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private Reject_Responseparser mReject_Responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MainActivity.this, "", "Rejecting...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mReject_Responseparser = gson.fromJson(response, Reject_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mReject_Responseparser != null) {
                if (mReject_Responseparser.code.trim().equals("200")) {

                    Toast.makeText(MainActivity.this, mReject_Responseparser.message, Toast.LENGTH_SHORT).show();

                } else {
                    CommonMethod.showAlert(mReject_Responseparser.message.toString().trim(),
                            MainActivity.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MainActivity.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.REJECT_REQUEST;
            Log.e("url----------------", "" + url);
            try {
                Reject_RequestParser mReject_RequestParser = new Reject_RequestParser();
                String R_driverid=mReject_RequestParser.driverId=driverides;
                String R_bookinid =mReject_RequestParser.bookingId=AcceptBooking;

                Log.e("Reject_Driverid----","" + R_driverid);
                Log.e("Reject_Bookingid---","" + R_bookinid);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mReject_RequestParser, Reject_RequestParser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MainActivity.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
