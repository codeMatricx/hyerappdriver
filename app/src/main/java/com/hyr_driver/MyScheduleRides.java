package com.hyr_driver;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.requestParser.MySchedule_Requestparser;
import com.hyr_driver.responseParser.MySchedule_Responseparser;
import com.hyr_driver.responseParser.RideLaterList;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;

import org.apache.http.entity.StringEntity;

import java.util.Calendar;
import java.util.List;

public class MyScheduleRides extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbartop;
    ListView listview;
    Button btnDatePicker, btnTimePicker,submit;
    EditText txtDate, txtTime;
    private int mYear, mMonth, mDay, mHour, mMinute;
    List<RideLaterList> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_my_schedule_rides);
        toolbartop = (Toolbar) findViewById(R.id.toolbar_myschedule);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        listview=(ListView)findViewById(R.id.myschedule_list);
        btnDatePicker=(Button)findViewById(R.id.btn_date);
        txtDate=(EditText)findViewById(R.id.in_date);
        submit=(Button)findViewById(R.id.submit_ridelater);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDate.getText().toString().length()==0){
                    txtDate.setError("Select Date");
                    txtDate.requestFocus();
                }
                
                else{
                    new MySchedule_job().execute();
                }

            }
        });

        btnDatePicker.setOnClickListener(this);
        

    }

    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

                            txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        
    }

    private class MySchedule_job extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private MySchedule_Responseparser mySchedule_responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(MyScheduleRides.this, "", "Please Wait...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mySchedule_responseparser = gson.fromJson(response, MySchedule_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mySchedule_responseparser != null) {
                if (mySchedule_responseparser.code.trim().equals("200")) {

                    MySchedulejob_Adapter mAll_Adapter = new MySchedulejob_Adapter(MyScheduleRides.this, mySchedule_responseparser.rideLaterList);
                    listview.setAdapter(mAll_Adapter);
                    mAll_Adapter.notifyDataSetChanged();

                } else {
                    CommonMethod.showAlert(mySchedule_responseparser.message.toString().trim(), MyScheduleRides.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), MyScheduleRides.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.MySchedulejob;
            Log.e("url----------------", "" + url);
            try {
               String driver_id= CommonMethod.getSavedPreferencesDriverID(MyScheduleRides.this);
                MySchedule_Requestparser mySchedule_requestparser = new MySchedule_Requestparser();
                String driverides=mySchedule_requestparser.driverId=driver_id;
                String bookingdate= mySchedule_requestparser.bookingDate=txtDate.getText().toString();


                Log.e("Driver_ides----","" + driverides);
                Log.e("Booking_Date---","" + bookingdate);



                Gson gson = new Gson();
                String requestInString = gson.toJson(mySchedule_requestparser, MySchedule_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(MyScheduleRides.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}
 