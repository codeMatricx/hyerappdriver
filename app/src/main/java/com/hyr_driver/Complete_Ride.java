package com.hyr_driver;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.hyr_driver.Extras.Constants;
import com.hyr_driver.Extras.DirectionFinder;
import com.hyr_driver.Extras.DirectionFinderListener;
import com.hyr_driver.Extras.Route;
import com.hyr_driver.requestParser.Accept_Requestparser;
import com.hyr_driver.requestParser.CompleteRide_Requestparser;
import com.hyr_driver.responseParser.Accept_Responseparser;
import com.hyr_driver.responseParser.CompleteRide;
import com.hyr_driver.responseParser.CompleteRide_Responseparser;
import com.hyr_driver.servicecall.ServiceCall;
import com.hyr_driver.utils.CommonMethod;
import com.hyr_driver.utils.Constant;
import com.hyr_driver.utils.Session;

import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Complete_Ride extends AppCompatActivity implements OnMapReadyCallback, PlaceSelectionListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, AdapterView.OnItemSelectedListener,DirectionFinderListener {
    private GoogleMap mMap;
    ArrayList<LatLng> MarkerPoints;
    Context context;
    PopupWindow popupWindow;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private static final String LOG_TAG = "PlaceSelectionListener";
    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    public GoogleMap mGoogleMap;
    private LatLng mLatLng;
    private double longitude1;
    double latitude;
    double longitude;
    private double latitude1;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Boolean isInternetPresent = false;
    Button btn_call,btn_arriveds,btn_ob_endtrip;
    FrameLayout container;
    Toolbar toolbartop;
    String src_latlang,address,source,destination,driverid,booking_ids,instant_source,instant_destination,original_fare;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_complete__ride);
        session=new Session(this);
        toolbartop = (Toolbar) findViewById(R.id.endtrip_toolbar);
        toolbartop.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backe));
        toolbartop.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        driverid=CommonMethod.getSavedPreferencesDriverID(Complete_Ride.this);
        source=CommonMethod.getSavedPreferencesNewSource(Complete_Ride.this);
        destination=CommonMethod.getSavedPreferencesNewDestination(Complete_Ride.this);
        btn_ob_endtrip=(Button)findViewById(R.id.btn_endtrip);
        btn_ob_endtrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.setSwitch(true);
                new Completeride().execute();

            }
        });
        booking_ids=CommonMethod.getSavedBookingIDMain(Complete_Ride.this);
        instant_source=CommonMethod.getSavedPreferencesNewSource(Complete_Ride.this);
        instant_destination=CommonMethod.getSavedPreferencesNewDestination(Complete_Ride.this);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Initializing
        MarkerPoints = new ArrayList<>();
        container = (FrameLayout) findViewById(R.id.map);
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();

            getUserLocation();
            initMap();
            if (mGoogleApiClient.isConnected() && mLastLocation != null) {
                startIntentService();
            }
            getGPSInfo();
        } else {
        }

        sendRequest();

    }


    private void sendRequest() {
        String origin = source;
        String destinations = destination;
        if (origin.isEmpty()) {
            Toast.makeText(this, "Please enter origin address!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (destinations.isEmpty()) {
            Toast.makeText(this, "Please enter destination address!", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            new DirectionFinder(this, origin, destinations).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void getGPSInfo() {
        Criteria criteria = new Criteria();
        String provider;
        Location location;
        LocationManager locationmanager = (LocationManager) Complete_Ride.this
                .getSystemService(Context.LOCATION_SERVICE);


        if (locationmanager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            provider = locationmanager.getBestProvider(criteria, false);
            if (ActivityCompat.checkSelfPermission(Complete_Ride.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Complete_Ride.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            location = locationmanager.getLastKnownLocation(provider);

        } else {
            showGPSDisabledAlertToUser();
        }


    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Complete_Ride.this);
        alertDialogBuilder
                .setMessage(
                        "GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings To Enable GPS",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);

                            }
                        });

        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();


    }

    private void startIntentService() {
        Intent intent = new Intent(this, Complete_Ride.class);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, mLastLocation);
        startService(intent);
    }

    private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getUserLocation();
    }

    //Getting current location
    private void getUserLocation() {
        //  mGoogleMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude1 = location.getLongitude();
            latitude1 = location.getLatitude();

            //moving the map to location
            moveMap();
        }
    }

    //Function to move the map
    public void moveMap() {
        //String to display current latitude and longitude
        String locationmsg = longitude1 + ", " + latitude1;

        //Creating a LatLng Object to store Coordinates
        LatLng slatLng = new LatLng(latitude1, longitude1);

        //Adding marker to map

        //Moving the camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(slatLng));

        //Animating the camera
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


    }


    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    protected void onResume() {
        super.onResume();


    }


    public boolean googleServiceAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(Complete_Ride.this, "can not connect play services", Toast.LENGTH_LONG).show();
        }
        return false;


    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        LatLng hcmus= new LatLng(28.579876,77.313748);
        mMap = googleMap;
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hcmus, 18));
//        originMarkers.add(mMap.addMarker(new MarkerOptions()
//                .title(address)
//                .position(hcmus)));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);


//
    }


    @Override
    public void onPlaceSelected(Place place) {

    }

    @Override
    public void onError(Status status) {

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", "entered");
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        latitude = location.getLatitude();
        longitude = location.getLongitude();
        src_latlang=(latitude+","+longitude);
        Log.e("latitude", "latitude--" + latitude);

        try {
            Log.e("latitude", "inside latitude--" + latitude);
            addresses = geocoder.getFromLocation(latitude, longitude, 1);


            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                //tv_source.setText(address + " " + city + " " + country);
                CommonMethod.saveRide_source(getApplicationContext(),address);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title(address);


        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location_green));
       // mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(9));
        Toast.makeText(Complete_Ride.this,"Your Current Location", Toast.LENGTH_LONG).show();

        // Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f",latitude,longitude));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }






    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }





    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 9));
            // ((TextView) findViewById(R.id.tv_etime)).setText(route.duration.text);
            //((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.caricon))
                    .title(route.startAddress)
                    .position(route.startLocation)));
            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_location))
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLACK).
                    width(6);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }
    private class Completeride extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;
        private CompleteRide_Responseparser mCompleteRide_Responseparser;
        private String response = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(Complete_Ride.this, "", "Completing...", true);
            progress.setCancelable(true);
        }

        @Override
        protected String doInBackground(String... params) {
            response = CallLoginService();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progress.dismiss();

            try {
                // get response from server side and store in SignupResponse Parser///////
                Gson gson = new Gson();
                mCompleteRide_Responseparser = gson.fromJson(response, CompleteRide_Responseparser.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JsonIOException e) {
                e.printStackTrace();
            }
            if (mCompleteRide_Responseparser != null) {
                if (mCompleteRide_Responseparser.code.trim().equals("200")) {
                    original_fare=mCompleteRide_Responseparser.completeRide.fare;
                    Intent intent = new Intent(Complete_Ride.this, Ratingbar.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    Toast.makeText(Complete_Ride.this, mCompleteRide_Responseparser.message, Toast.LENGTH_SHORT).show();

                    CommonMethod.saveOriginalFare(getApplicationContext(),original_fare);

                } else {
                    CommonMethod.showAlert(mCompleteRide_Responseparser.message.toString().trim(),
                            Complete_Ride.this);
                }
            } else {
                CommonMethod.showAlert(getResources().getString(R.string.connection_error), Complete_Ride.this);
            }
        }

        @SuppressLint("DefaultLocale")

        //Data Parse to Server user'Data and return response /////////////////////////////////////////////////////////////

        private String CallLoginService() {
            String url = Constant.BASE_URL + Constant.COMPLETE_RIDE;
            Log.e("url----------------", "" + url);
            try {
                String Booking= CommonMethod.getSavedBookingIDMain(Complete_Ride.this);
                CompleteRide_Requestparser mCompleteRide_Requestparser = new CompleteRide_Requestparser();
                String driverids=mCompleteRide_Requestparser.driverId=driverid;
                String bookinid =mCompleteRide_Requestparser.bookingId=Booking;
                String sources=mCompleteRide_Requestparser.source=instant_source;
                String destinations=mCompleteRide_Requestparser.destination=instant_destination;

                Log.e("Driverid----","" + driverids);
                Log.e("Bookingid---","" + bookinid);
                Log.e("Booking_instant---","" + Booking);
                Log.e("Sources_instant---","" + sources);
                Log.e("Destinations_instant---","" + destinations);

                Gson gson = new Gson();
                String requestInString = gson.toJson(mCompleteRide_Requestparser, CompleteRide_Requestparser.class);
                System.out.println(requestInString);
                StringEntity stringEntity = new StringEntity(requestInString);
                ServiceCall serviceCall = new ServiceCall(Complete_Ride.this, url, stringEntity);
                response = serviceCall.getServiceResponse();
                Log.e("Response -- ", "" + response);
                System.out.println(serviceCall.getServiceResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }
}

